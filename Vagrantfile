# -*- mode: ruby -*-
# vi: set ft=ruby :

$provisionning = <<-PROVISIONNING

# Astellia certificate in rootCA
sudo tee /etc/pki/ca-trust/source/anchors/astellia.der <<B64DER
-----BEGIN CERTIFICATE-----
MIIDrzCCApegAwIBAgIQctBLdKkxF5RJpOn2dgJ3YTANBgkqhkiG9w0BAQ0FADBL
MRMwEQYKCZImiZPyLGQBGRYDbG9jMRgwFgYKCZImiZPyLGQBGRYIYXN0ZWxsaWEx
GjAYBgNVBAMTEWFzdGVsbGlhLUFTVERDLUNBMB4XDTE2MDYyMzA4Mjg1OFoXDTM2
MDYyMzA4Mzg1NlowSzETMBEGCgmSJomT8ixkARkWA2xvYzEYMBYGCgmSJomT8ixk
ARkWCGFzdGVsbGlhMRowGAYDVQQDExFhc3RlbGxpYS1BU1REQy1DQTCCASIwDQYJ
KoZIhvcNAQEBBQADggEPADCCAQoCggEBANsG7eaknz2W9CT4zL7ytdEXLyVPF555
7T34G5FyChdHHhREmesy2qJ7hvFuWrZkzigWBf5rVKQrv9qOppHD5lnb3H5bGPD2
hMutCmAWOKpFrk7LJmlJF9unwSzqnqN/UTSCSFs8U1+C2kB4aX5HuS9D1s61h+e2
P7z+pr16XCeWhuXBMVkng3xsGojVE54gPnNHd1c0dzIwahZSXEQg24k5Wz0nkw3e
hzb4d6JutjHhiE92TqmNOhJnTk05mAfk4z1aM5Qzkc9JZWzoVP5tr+qfL4h9mn4G
Qdm8j5og9Ed6I0SmIf6nf1FfcQy2XMUTMGNXz6K5KQnnk/sFPXy27cECAwEAAaOB
jjCBizATBgkrBgEEAYI3FAIEBh4EAEMAQTALBgNVHQ8EBAMCAYYwDwYDVR0TAQH/
BAUwAwEB/zAdBgNVHQ4EFgQUXY2Sq/UMLSpwowKd7mQmLrPwCHMwEgYJKwYBBAGC
NxUBBAUCAwIAAjAjBgkrBgEEAYI3FQIEFgQUGjOjaNWN6iGVbIYKye99MMbmsJ8w
DQYJKoZIhvcNAQENBQADggEBAETy4f/UliraALaQylynbzAOrJIvpmrGqAuSYoFB
XcIzAvGvOTvVsSXnqvDcA2NhHQamOJ5X2J4DF8YKKWWF0Z2oW+B3HIb+B+yr8UKU
zrJLboprAu/pJYFEm+2XSvJIB+P0SqSPX2SEhPDP8Fx+rlZFHBsijdLCJumnhUdk
zehD3wxhrwGUbVq3zCFYzIupPIvPNTFqeSIA5C2DdkLki+qFfzhrDdxk0lYgKlnx
3SXnW7YeUarreCvnyPlwFCKTuGkpp9QD1cSXPjeHRvUmQBMx13fxK27fdJ2KQhw8
9cmrzgLFeK2FJBDH5dIgPLHpzlL5NtoLThAx9OnMu1VyM74=
-----END CERTIFICATE-----
B64DER
sudo update-ca-trust enable
sudo update-ca-trust extract

# Installing tools
sudo yum -y install epel-release
sudo yum install -y curl dos2unix git make mlocate nmap strace tar tee tree unzip vim
sudo updatedb

# Installing Docker
sudo curl -fsSL get.docker.com | bash
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -a -G docker vagrant

# Need python >= 3.5. And gcc is required by some packages installed by pip
sudo yum -y install https://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-15.ius.centos7.noarch.rpm
sudo yum -y install python36u python36u-pip python36u-devel gcc
sudo pip3.6 install --upgrade pip ansible docker docker-compose molecule

# Disabling SELinux
sudo setenforce 0
sudo sed -i 's;SELINUX=enforcing;SELINUX=disabled;' /etc/selinux/config

echo
echo "All set. Now just do"
echo
echo "    vagrant reload"
echo "    vagrant ssh"
echo "    cd /vagrant/ansible-role-YOUR_ROLE_NAME"
echo "    molecule test"
echo
echo " "

PROVISIONNING

Vagrant.configure(2) do |config|
  config.vm.box = "astellia/centos-7"
  config.vm.box_url = 'https://nexus.astellia.loc/content/sites/packer/centos-7-x86_64.virtualbox.box'
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"
    vb.cpus = "1"
  end

  config.vm.network "private_network", ip: "192.168.111.110"
  config.vm.hostname = 'ansible.vagrant'
  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
  end

  config.vm.synced_folder "..", "/vagrant", type: "virtualbox"
  config.vm.provision "shell", inline: $provisionning
end
