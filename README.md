# Ansible ESXi

POC about managing ESXi's host via Ansible

## About Ansible official vmware modules

**TL;DR : use vmware_guest + vmware_copy, that's all**

Here's the Ansible documentation for VMWare modules : https://docs.ansible.com/ansible/latest/modules/list_of_cloud_modules.html#vmware. Warnings :

- It's a lot about managing a VMWare infrastructure through vCenter API, few of those are relevant for standalone ESXi
- Most modules are "Tested on vSphere 5.5, 6.0 and 6.5", which exclude 5.1
- As a matter of fact, ESXi 5.1 UI doesn't even have an option to create a VM from a template...

Some Q&A on the topic, not really relevant because it uses deprecated vsphere_guest

- https://stackoverflow.com/questions/43420285/can-i-manage-guest-vms-in-vsphere-free-using-ansible-without-vcenter
- https://stackoverflow.com/questions/45327563/how-to-create-vm-on-standalone-esxi-hosts-using-vsphere-guest-ansible-module
- https://pastebin.com/sQ2XMBXe

## So, how ?

From my research, one can upload a customized on the fly ISO including a kickstart, or a standard iso plus a kickstart iso.

See `playbook.yml` for an attempt at this.

## Another inspiration ?

A (very good ?) inspiration with heavy scripting passed through ansible https://github.com/veksh/ansible-esxi
