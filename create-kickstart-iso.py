#!/usr/bin/env python

"""
Create a kickstart iso [arg2] given kickstart file [arg1]

Doc :
- https://github.com/ansible/ansible/issues/35665#issuecomment-375859256
- https://clalancette.github.io/pycdlib/examples.html
"""

import sys
import pycdlib

iso = pycdlib.PyCdlib()
iso.new(vol_ident="OEMDRV", rock_ridge="1.09")

iso.add_file(sys.argv[1], "/KS.CFG;1", rr_name="ks.cfg")
iso.write(sys.argv[2])
iso.close()
